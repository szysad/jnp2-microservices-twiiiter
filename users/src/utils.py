from contextlib import contextmanager
from .app import SessionLocal
from sqlalchemy.orm.session import Session


@contextmanager
def session_scope() -> Session:
    '''provides transaction scope of session'''
    session = SessionLocal()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
