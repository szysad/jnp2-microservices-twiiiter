from flask import Flask
from typing import Tuple
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, session


app = Flask(__name__)


def init_db() -> session:
    ''' inits db connection and tables '''
    from . import config

    DB_URL = (
        f"postgres://{config.DB_USER}:{config.DB_PASS}"
        f"@{config.DB_HOST}/{config.DB_DATABASE}"
    )

    engine = create_engine(DB_URL, echo=config.DEBUG)
    sess = sessionmaker(bind=engine)
    # create models
    from .models import Base
    Base.metadata.create_all(bind=engine)

    return sess


def init_routes() -> None:
    '''inits routes'''
    from . import routes


SessionLocal = init_db()
init_routes()
print('app started')
