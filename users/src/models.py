import sqlalchemy as sa  # type: ignore
import jwt
from typing import Dict
# https://github.com/dropbox/sqlalchemy-stubs/issues/76
from sqlalchemy.ext.declarative import DeclarativeMeta  # type: ignore
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from .config import TOKEN_SECRET
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class User(Base):
    '''User model'''
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(), nullable=False, unique=True)
    password = sa.Column(sa.String(), nullable=False)

    def __repr__(self):
        return f"<User(id='${self.id}', name='${self.name}')>"

    def __init__(self, name: str, password: str):
        self.password = generate_password_hash(password)
        self.name = name

    def encode_jwt(self) -> str:
        '''
            Generates jwt auth token,
            can throw any child of jwt.exceptions
        '''

        payload = {
            'exp': datetime.utcnow() + timedelta(days=1),
            'iat': datetime.utcnow(),
            'sub': self.id
        }

        return jwt.encode(
            payload,
            TOKEN_SECRET,
            algorithm='HS256'
        ).decode("utf-8")

    def matches_hash(self, password: str) -> bool:
        ''' check if given password matches user hashed password '''
        return check_password_hash(self.password, password)

    @staticmethod
    def decode_jwt(token: str) -> Dict[str, str]:
        '''
            returns jwt token payload or raises except
            of base jwt.exceptions.InvalidTokenError
        '''
        return jwt.decode(token, TOKEN_SECRET)

    def serialize(self, *fields) -> Dict[str, str]:
        rez = {}
        for field in fields:
            if (val := self.__dict__.get(field)) is None:
                raise Exception(f"class User doesnt have attribute '{field}'")
            else:
                rez[field] = val

        return rez
