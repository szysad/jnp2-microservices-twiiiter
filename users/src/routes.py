from .app import app
from .models import User
from .decorators import require_json_fields, require_query_params
from flask import request, jsonify
from typing import List, Callable, Dict
from .utils import session_scope


@app.route('/users', methods=['POST'])
@require_json_fields('name', 'password')
def register_user():
    '''
        registers new user in database and returns user auth token
        header content type should be aplication/json
    '''

    for key, val in request.json.items():
        if not isinstance(val, str):
            return {
                       "error": f"value of {key} has to be string"
                   }, 400

    name = request.json['name']
    password = request.json['password']

    for key, val in request.json.items():
        if not isinstance(val, str):
            return {
                       "error": f"value of {key} has to be string"
                   }, 400

    with session_scope() as sess:
        if sess.query(User).filter_by(name=name).count() > 0:
            # this username is already taken
            return {
                       "error": f"username '{name}' is already taken"
                   }, 409  # conflict status code

        new_user = User(name, password)
        sess.add(new_user)
        sess.flush()
        sess.refresh(new_user)
        return {"token": new_user.encode_jwt()}


@app.route('/users/get-token', methods=['POST'])
@require_json_fields('name', 'password')
def get_token():
    '''
        returns user's token and data based on user's credentials
    '''

    name = request.json.get('name')
    password = request.json.get('password')

    with session_scope() as sess:
        user = sess.query(User).filter_by(name=name).first()

        if user is None or not user.matches_hash(password):
            return {
                       "error": "user with such name or password doesn't exist"
                   }, 404

        return {"token": user.encode_jwt()}


@app.route('/users', methods=['GET'])
@require_query_params('id')
def get_users():
    '''
        returns data of users with given ids
    '''
    raw_ids = request.args.getlist('id')
    ids: List[int] = []
    for raw_id in raw_ids:
        try:
            ids.append(int(raw_id))
        except Exception:
            return {"error": "value of key 'id' must be integer"}

    with session_scope() as sess:
        users = sess.query(User).filter(User.id.in_(ids)).all()
        if users is None:
            return []

        return jsonify([user.serialize('name', 'id') for user in users])


@app.route('/users/get-id', methods=['GET'])
@require_json_fields('name')
def get_user_id():
    '''
        returns id of user with given name
    '''
    name = request.json.get('name')
    with session_scope() as sess:
        user = sess.query(User).filter_by(name=name).first()
        if user is None:
            return ""
        return str(user.id)
