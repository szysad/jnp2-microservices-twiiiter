import os


DB_HOST = os.getenv('DB_HOST')  # addr:port
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')
DB_DATABASE = os.getenv('DB_DATABASE')
TOKEN_SECRET = os.getenv('TOKEN_SECRET')
DEBUG = True if os.getenv('DEBUG') is not None else False


def env_check():
    envs = [(DB_HOST, "DB_HOST"),
            (DB_USER, "DB_USER"),
            (DB_PASS, "DB_PASS"),
            (DB_DATABASE, "DB_DATABASE"),
            (TOKEN_SECRET, "TOKEN_SECRET")]

    for env_var, var_name in envs:
        if env_var is None:
            raise Exception(f"mandatory variable '{var_name}' not set")


env_check()
