from functools import wraps
from flask import request, jsonify


def require_json_fields(*required: str):
    '''
        requires that in decorated view function request.json.keys()
        contains given fields
    '''
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if request.json is None:
                return {"error": "no json data received"}, 400
            keys = request.json.keys()
            for mandatory in required:
                if (mandatory not in keys):
                    msg = {"error": f"argument '{mandatory}' not given"}
                    return jsonify(msg), 400
            return func(*args, *kwargs)
        return wrapper
    return decorator


def require_query_params(*required: str):
    '''
        requires that in decorated view function request.args.keys()
        contains given fields
    '''
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            keys = request.args.keys()
            for mandatory in required:
                if (mandatory not in keys):
                    msg = {"error": f"argument '{mandatory}' not given"}
                    return jsonify(msg), 400
            return func(*args, *kwargs)
        return wrapper
    return decorator
