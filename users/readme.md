# Users service
 - generates JWT auth tokens
 - keeps user's data in database
***
# Endpoints
### POST /users
Request header `Content-Type` type must be `application/json`
Requests data must be json data with fields `name` and `password` populated with strings

Example request:
```sh
$ CURL -X POST -H "Content-Type: application/json" -d '{"name": "user1", "password": "password1"}'
```
Example response:
```json
{
    "token": "nlkjnwdlknldfniu.ewjfoqerfndskngfkjdsngksnfgndsnglkjdsgdsnsgnsasmc.kjsanclkjsanclksaclkjanc"
}
```

### POST /users/get-token
Request header `Content-Type` type must be `application/json`
Requests data must be json data with fields `name` and `password` populated with strings
Example request:
```sh
$ CURL -X POST -H "Content-Type: application/json" -d '{"name": "user1", "password": "password1"}'
```
Example response:
```json
{
    "token": "nlkjnwdlknldfniu.ewjfoqerfndskngfkjdsngksnfgndsnglkjdsgdsnsgnsasmc.kjsanclkjsanclksaclkjanc"
}
```