const qs = require('qs');

function create_config(req, params: {} = {}) { // creates axio config object given route 'req' var
    return {
        headers: req.headers,
        validateStatus: status => true, // accept all status codes
        params: params,
        paramsSerializer: params => {
            // formats query lists, for example {key: ['x', 'y', 'z']} into ?key=x&key=y&key=z
            return qs.stringify(params, {arrayFormat: 'repeat'});
        }
    }
}

function success_status(status: number): boolean {
    if (status < 200) return false;
    if (status >= 300) return false;
    return true;
}

function original_url(req): URL {
    return new URL(req.protocol + '://' + req.get('host') + req.originalUrl);
}

export {create_config, success_status, original_url}
