import axios, { AxiosResponse } from "axios"
import {create_config, original_url} from "./utils"

function accept_only_json_type(req, res, next) {
    if (req.method === 'POST' && !req.is('application/json')) {
        return res.status(400).send(
            {"error": "request content type must be 'application/json'"}
        )
    }
    next();
}

// HTTP methods [GET, POST] implemented
function simple_proxy(inner_host: string): (req, res, next) => any {
    let func = async (req, res, next) => {
        let inner_url = original_url(req);
        inner_url.host = inner_host;

        let inner_res_func: () => Promise<AxiosResponse<any>>
        switch (req.method) {
            case 'GET': {
                inner_res_func = () => axios.get(inner_url.href, create_config(req));
                break;
            }
            case 'POST': {
                inner_res_func = () => axios.post(inner_url.href, req.body, create_config(req));
                break;
            }
            default: {
                throw new Error(`HTTP method ${req.method} not implemented by simple_proxy`)
            }
        }
        try {
            const inner_res = await inner_res_func();
            res.set(inner_res.headers);
            return res.status(inner_res.status).send(inner_res.data);
        } catch (error) {
            console.log(error);
            return res.status(500).send();
        }
    }
    return func;
}

export {accept_only_json_type, simple_proxy}