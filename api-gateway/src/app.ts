const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
import {router as users_router} from "./routes/users"
import {router as posts_router} from "./routes/posts"
import {router as relations_router} from "./routes/relations"
import {router as messages_router} from "./routes/messages"
import {PORT, RELATIONS_SERVICE} from "./config"
import {accept_only_json_type} from "./middleware"
import {POSTS_SERVICE, USERS_SERVICE} from "./config"
import {MESSAGES_SERVICE} from "./config"


const app = express()
app.use(morgan('combined'));
app.use(accept_only_json_type);
app.use(bodyParser.json({
    type: () => true
}));

app.use('/users', users_router);
app.use('/posts', posts_router);
app.use('/relations', relations_router);
app.use('/messages', messages_router);

app.listen(PORT, () => {
    console.log(`api-gateway started on port ${PORT}!`);
})

export const POSTS_BASE_URL = `http://${POSTS_SERVICE}/posts`;
export const USERS_BASE_URL = `http://${USERS_SERVICE}/users`;
export const RELATIONS_BASE_URL = `http://${RELATIONS_SERVICE}/relations`;
export const MESSAGES_BASE_URL = `http://${MESSAGES_SERVICE}/messages`;
