const detenv = require('dotenv').config()


const USERS_SERVICE = process.env.USERS_SERVICE
const POSTS_SERVICE = process.env.POSTS_SERVICE
const RELATIONS_SERVICE = process.env.RELATIONS_SERVICE
const MESSAGES_SERVICE = process.env.MESSAGES_SERVICE
const TOKEN_SECRET = process.env.TOKEN_SECRET
const PORT = 5000


function check_env() {
    const env_vars: [string, string][] = [
        [USERS_SERVICE, "USERS_SERVICE"],
        [POSTS_SERVICE, "POSTS_SERVICE"],
        [RELATIONS_SERVICE, "RELATIONS_SERVICE"],
        [MESSAGES_SERVICE, "MESSAGES_SERVICE"]];

    for (const env_var of env_vars) {
        if (env_var[0] === undefined) {
            throw new Error(`mandatory enviroment variable ${env_var[1]} is not set`);
        }
    }
}

check_env()

export {USERS_SERVICE, POSTS_SERVICE, PORT, RELATIONS_SERVICE, MESSAGES_SERVICE}