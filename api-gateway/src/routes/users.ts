const express = require('express')
import {simple_proxy} from "../middleware"
import {USERS_SERVICE} from "../config"


// we are in <schema>:<host>/users
let router = express.Router()
// enable simple proxy
router.use(simple_proxy(USERS_SERVICE));


router.post("");

router.post("/get-token");


export {router}
