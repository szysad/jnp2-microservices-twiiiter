const express = require('express')

import axios, {AxiosRequestConfig} from "axios"
import {MESSAGES_BASE_URL, USERS_BASE_URL} from "../app";
import {create_config, success_status} from "../utils";

// we are in <schema>:<host>/messages
const router = express.Router();

// adds new message
router.post("", async (req, res) => {
    try {
        let config : AxiosRequestConfig = {
            method: 'post',
            headers: req.headers,
            data: req.body
        }
        const id_res = await axios.get(USERS_BASE_URL + '/get-id', config);
        if (!success_status(id_res.status))
            return res.status(id_res.status).send(id_res.data);
        let receiver_id = id_res.data;

        if (receiver_id === "")
            return res.status(200).send({"error": "user not exist"});

        config = {
            method: 'post',
            headers: req.headers
        }

        const messages_res = await axios.post(MESSAGES_BASE_URL, {"content": req.body.content, "receiver_id": receiver_id}, config);
        return res.status(messages_res.status).send(messages_res.data);
    } catch (error) {
        console.log(error);
        return res.status(500).send();
    }
});

router.get("", async (req, res) => {
    try {
        const messages_res = await axios.get(MESSAGES_BASE_URL, create_config(req, req.query));
        if (!success_status(messages_res.status))
            return res.status(messages_res.status).send(messages_res.data);

        let sender_ids: number[] = [];
        for (const message of messages_res.data)
            sender_ids.push(message.sender_id);

        if (sender_ids.length === 0)
            return res.status(messages_res.status).send([]);

        const users_res = await axios.get(USERS_BASE_URL, create_config(req, {
            id: sender_ids
        }));

        if (!success_status(users_res.status))
            return res.status(users_res.status).send(users_res.data);

        const users_set: Map<number, string> = new Map();
        for (const user of users_res.data)
            users_set.set(user.id, user.name)

        for (const message of messages_res.data) {
            message.from = users_set.get(message.sender_id);
            delete message.sender_id;
            delete message.receiver_id;
        }
        return res.status(users_res.status).send(messages_res.data);
    } catch (error) {
        console.log(error);
        return res.status(500).send();
    }
})


export {router}