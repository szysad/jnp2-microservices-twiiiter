import axios, {AxiosRequestConfig} from "axios";

const express = require('express')
import {RELATIONS_BASE_URL, USERS_BASE_URL} from "../app";
import {success_status} from "../utils";
import {simple_proxy} from "../middleware";
import {RELATIONS_SERVICE} from "../config";


// we are in /relations
const router = express.Router();

router.post("", async (req, res) => {
    try {
        let config : AxiosRequestConfig = {
            method: 'post',
            headers: req.headers,
            data: req.body
        }

        const id_res = await axios.get(USERS_BASE_URL + '/get-id', config);
        if (!success_status(id_res.status))
            return res.status(id_res.status).send(id_res.data);

        let user_id = id_res.data
        if (user_id === "")
            return res.status(200).send({"error": "user not exist"});

        config = {
            method: 'post',
            headers: req.headers
        }

        const relations_res = await axios.post(RELATIONS_BASE_URL, {'followed': user_id}, config);
        return res.status(relations_res.status).send(relations_res.data);
    } catch (error) {
        console.log(error);
        return res.status(500).send();
    }
});

router.get("/followers", simple_proxy(RELATIONS_SERVICE));

router.get("/followed", simple_proxy(RELATIONS_SERVICE));


export {router}
