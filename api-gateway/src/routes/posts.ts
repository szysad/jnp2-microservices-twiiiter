const express = require('express')
import axios from "axios"
import {POSTS_BASE_URL, RELATIONS_BASE_URL, USERS_BASE_URL} from "../app"
import {create_config, success_status} from "../utils"
import {POSTS_SERVICE} from "../config"
import {simple_proxy} from "../middleware"


// we are in <schema>:<host>/posts
const router = express.Router()
const UNKNOWN_USER = 'unknown';

// adds new post
router.post("", simple_proxy(POSTS_SERVICE));

// returns queried posts
router.get("", async (req, res) => {
    try {/*
        const posts_res = await axios.get(POSTS_BASE_URL, create_config(req, req.query));
        if (!success_status(posts_res.status))
            return res.status(posts_res.status).send(posts_res.data);
        
        let author_ids: number[] = [];
        for (const post of posts_res.data)
            author_ids.push(post.author_id);
        
        if (author_ids.length === 0)
            return res.status(posts_res.status).send([]);

        const users_res = await axios.get(USERS_BASE_URL, create_config(req, {
            id: author_ids
        }));

        if (!success_status(users_res.status))
            return res.status(users_res.status).send(users_res.data);

        const users_set: Map<number, string> = new Map();
        for (const user of users_res.data)
            users_set.set(user.id, user.name)

        for (const post of posts_res.data) {
            const id = post.author_id;
            if (users_set.has(id))
                post.author = users_set.get(id);
            else
                post.author = UNKNOWN_USER;
            delete post.author_id;
        }
        return res.status(users_res.status).send(posts_res.data);*/
        const relations_res = await axios.get(RELATIONS_BASE_URL + '/followed', create_config(req, req.query));
        if (!success_status(relations_res.status))
            return res.status(relations_res.status).send(relations_res.data);

        if (relations_res.data.length == 0)
            return res.status(200).send([]);

        const posts_res = await axios.get(POSTS_BASE_URL, create_config(req, {author_id: relations_res.data}));
        if (!success_status(posts_res.status))
            return res.status(posts_res.status).send(posts_res.data);

        const users_res = await axios.get(USERS_BASE_URL, create_config(req, {id: relations_res.data}));
        if (!success_status(users_res.status))
            return res.status(users_res.status).send(users_res.data);

        const users_set: Map<number, string> = new Map();
        for (const user of users_res.data)
            users_set.set(user.id, user.name)

        for (const post of posts_res.data) {
            const id = post.author_id;
            if (users_set.has(id))
                post.author = users_set.get(id);
            else
                post.author = UNKNOWN_USER;
            delete post.author_id;
        }
        return res.status(users_res.status).send(posts_res.data);
    } catch (error) {
        console.log(error);
        return res.status(500).send();
    }
})

export {router}
