const detenv = require('dotenv').config()

// todo zmien to na cos odpowiedniego
const API_SERVICE = process.env.API_SERVICE

const POSTS_BASE_URL = `http://${API_SERVICE}/posts`;
const USERS_BASE_URL = `http://${API_SERVICE}/users`;
const RELATIONS_BASE_URL = `http://${API_SERVICE}/relations`;
const MESSAGES_BASE_URL = `http://${API_SERVICE}/messages`;

var express = require('express');
var router = express.Router();
var createError = require('http-errors');
var request = require('request');
const axios = require('axios');

var csrf = require('csurf');
var bodyParser = require('body-parser');
var csrfProtection = csrf({cookie: true})
var parseForm = bodyParser.urlencoded({extended: false})

function descend_sort(a, b) {
    return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
}

async function get_feed(jwt) {
    const config = {
        method: 'get',
        url: POSTS_BASE_URL,
        headers: {
            'content-type': 'application/json',
            'WWW-Authenticate': jwt
        }
    }
    try {
        const response = await axios(config);
        return response.data.sort(descend_sort).slice(0, 5);
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function get_messages(jwt) {
    const config = {
        method: 'get',
        url: MESSAGES_BASE_URL,
        headers: {
            'content-type': 'application/json',
            'WWW-Authenticate': jwt
        }
    }
    try {
        const response = await axios(config);
        return response.data.sort(descend_sort).slice(0, 5);
    } catch (error) {
        console.log(error);
        return null;
    }
}

/* GET home page. */
router.get('/', csrfProtection, function (req, res, next) {
    res.render('index', {csrfToken: req.csrfToken()});
});

router.get('/signup', csrfProtection, function (req, res, next) {
    res.render('signup', {csrfToken: req.csrfToken()});
});

router.post('/', parseForm, csrfProtection, function (req, res, next) {
    const data = {
        'name': req.body.username,
        "password": req.body.password
    }
    const options = {
        url: USERS_BASE_URL + "/get-token",
        headers: {
            'Content-type': 'application/json'
        },
        method: "POST",
        body: data,
        json: true
    }
    const r = request.post(options, async function (err, resp, body) {
        if (err) {
            res.locals.message = true;
            res.render('index', {csrfToken: req.csrfToken(), msg: "Connection error"});
        } else if (resp.statusCode !== 200) {
            res.locals.message = true;
            const msg = resp.statusCode === 404 ? "Invalid username or password" : "Internal error";
            res.render('index', {csrfToken: req.csrfToken(), msg: msg});
        } else {
            res.cookie("jwt", body.token);
            res.redirect('/app');
        }
    });
});

router.post('/signup', parseForm, csrfProtection, function (req, res, next) {
    const data = {
        'name': req.body.username,
        "password": req.body.password
    }
    const options = {
        url: USERS_BASE_URL,
        headers: {
            'Content-type': 'application/json'
        },
        method: "POST",
        body: data,
        json: true
    }
    const r = request.post(options, function (err, resp, body) {
        if (err) {
            res.locals.message = true;
            res.render('signup', {csrfToken: req.csrfToken(), msg: "Connection error"});
        } else if (resp.statusCode !== 200) {
            res.locals.message = true;
            const msg = resp.statusCode === 409 ? "Username already taken" : "Internal error";
            res.render('signup', {csrfToken: req.csrfToken(), msg: msg});
        } else {
            res.cookie("jwt", body.token);
            res.render('app', {csrfToken: req.csrfToken(), messages: [], feed: []});
        }
    });
});

router.get('/app', csrfProtection, async function (req, res, next) {
    let jwt = req.cookies.jwt;

    if (jwt === undefined) {
        res.locals.message = true;
        res.render('index', {csrfToken: req.csrfToken(), msg: "Unauthorized user"});
    }

    let messages = await get_messages(jwt);
    if (messages === null) {
        res.locals.message = true;
        res.render('index', {csrfToken: req.csrfToken(), msg: "Unauthorized user"});
    }
    let feed = await get_feed(jwt);
    if (feed === null) {
        res.locals.message = true;
        res.render('index', {csrfToken: req.csrfToken(), msg: "Unauthorized user"});
    }
    res.render('app', {csrfToken: req.csrfToken(), messages: messages, feed: feed});
})

router.post('/app', parseForm, csrfProtection, async function (req, res, next) {
    let messages = JSON.parse(req.body.messages);
    let feed = JSON.parse(req.body.feed);
    res.locals.message = true;
    if (req.body.type === "newFriend") {
        const config = {
            method: 'post',
            url: RELATIONS_BASE_URL,
            headers: {
                'content-type': 'application/json',
                'WWW-Authenticate': req.cookies.jwt
            },
            data: {
                'name': req.body.user
            }
        }
        try {
            const response = await axios(config);
            let msg;
            if (response.data.error !== undefined)
                msg = response.data.error;
            else
                msg = response.data.msg;
            res.render('app', {csrfToken: req.csrfToken(), msg: msg, messages: messages, feed: feed});
        } catch (error) {
            res.render('app', {csrfToken: req.csrfToken(), msg: "Internal error", messages: messages, feed: feed});
        }
    } else if (req.body.type === "newPost") {
        const config = {
            method: 'post',
            url: POSTS_BASE_URL,
            headers: {
                'content-type': 'application/json',
                'WWW-Authenticate': req.cookies.jwt
            },
            data: {
                'content': req.body.post
            }
        }
        try {
            const response = await axios(config);
            res.render('app', {
                csrfToken: req.csrfToken(),
                msg: "Post successfully added",
                messages: messages,
                feed: feed
            });
        } catch (error) {
            res.render('app', {csrfToken: req.csrfToken(), msg: "Internal error", messages: messages, feed: feed});
        }
    } else if (req.body.type === "newMessage") {
        const config = {
            method: 'post',
            url: MESSAGES_BASE_URL,
            headers: {
                'content-type': 'application/json',
                'WWW-Authenticate': req.cookies.jwt
            },
            data: {
                'content': req.body.message_content,
                'name': req.body.message_to
            }
        }
        try {
            let msg;
            const response = await axios(config);
            if (response.data.error !== undefined)
                msg = response.data.error;
            else
                msg = "Message sent";
            res.render('app', {csrfToken: req.csrfToken(), msg: msg, messages: messages, feed: feed});
        } catch (error) {
            res.render('app', {csrfToken: req.csrfToken(), msg: "Internal error", messages: messages, feed: feed});
        }
    } else if (req.body.type === "logout") {
        res.clearCookie("jwt");
        res.redirect('/');
    } else {
        console.log(req.body.type);
        next(createError(500));
    }
})

module.exports = router;
