from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa
from datetime import datetime
from typing import Dict


Base = declarative_base()


class Following(Base):
    __tablename__ = 'follow'

    #  multi column primary key
    follower = sa.Column(sa.Integer, primary_key=True)
    followed = sa.Column(sa.Integer, primary_key=True)

    def __init__(
            self,
            follower: int,
            followed: int):
        self.follower = follower
        self.followed = followed

    def __repr__(self):
        return f"Follow<(follower={self.follower}, followed={self.followed}"

    def serialize(self) -> Dict[str, str]:
        return {"follower": self.follower, "followed": self.followed}
