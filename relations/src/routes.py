from .app import app
from .models import Following
from .utils import session_scope, extract_id, InvalidTokenError
from fastapi import APIRouter, Response, status, Query, Header, Body

router = APIRouter()


@router.post("/relations")
def follow(followed: int = Body(..., gt=0, embed=True), WWW_Authenticate: str = Header(...)):
    try:
        follower = extract_id(WWW_Authenticate)
    except InvalidTokenError as e:
        print(e)
        Response.status_code = status.HTTP_403_FORBIDDEN
        return {"error": "invalid token"}

    if followed == follower:
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return {"error": "user can not follow himself"}

    with session_scope() as sess:
        if sess.query(Following).filter_by(follower=follower, followed=followed).count() > 0:
            Response.status_code = status.HTTP_409_CONFLICT
            return {"error": "user already followed"}

        new_follow = Following(follower=follower, followed=followed)
        sess.add(new_follow)
    return {"msg": "user succesfully followed"}


@router.get("/relations/followers")
def get_followers(id: int = Query(..., gt=0)):
    with session_scope() as sess:
        followers = sess.query(Following).filter_by(followed=id).all()
        print(followers)
        return [follow.follower for follow in followers]


@router.get("/relations/followed")
def get_followed(WWW_Authenticate: str = Header(...)):
    id: int
    try:
        id = extract_id(WWW_Authenticate)
    except InvalidTokenError as e:
        print(e)
        Response.status_code = status.HTTP_403_FORBIDDEN
        return {"error": "invalid token"}

    with session_scope() as sess:
        followed = sess.query(Following).filter_by(follower=id).all()
        return [follow.followed for follow in followed]
