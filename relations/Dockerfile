FROM python:3.8.3-slim-buster as builder

LABEL builder=true

WORKDIR /app

COPY . .

# why pipenv from this version https://github.com/pypa/pipenv/issues/4220
# --force-reinstall to deal with already installed packages like 'cerify'
# --user so all packages go to ~/.local/
RUN pip install pipenv==2018.11.26 --no-cache-dir && \
    pipenv lock -r > requirements.txt && \
    pip uninstall -y pipenv && \
    rm -f Pipfile Pipfile.lock && \
    pip install --force-reinstall --user -r requirements.txt

# next stage build
FROM python:3.8.3-slim-buster

COPY --from=builder /root/.local /root/.local
COPY --from=builder /app /app
# now we have /app/src/app.py

ENV PATH=/root/.local/bin:$PATH

WORKDIR /app

CMD ["gunicorn", "-w", "1", "-k", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:5000", "--access-logfile", "-", "src.app:app"]
