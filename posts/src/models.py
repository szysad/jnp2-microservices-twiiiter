from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa
from datetime import datetime
from typing import Dict


CONTENT_MAX_LEN = 280
Base = declarative_base()


class Post(Base):
    __tablename__ = 'posts'

    id = sa.Column(sa.INTEGER, primary_key=True)
    author_id = sa.Column(sa.Integer, nullable=False)
    content = sa.Column(sa.String(CONTENT_MAX_LEN), nullable=False)
    created_at = sa.Column(sa.DateTime, nullable=False)

    def __init__(
            self,
            author_id: int,
            content: str):
        self.author_id = author_id
        self.content = content
        self.created_at = datetime.now()

    def __repr__(self):
        return (f"Post<(id={self.id}, author_id={self.author_id},",
                f"created_at={self.created_at},",
                f"content={self.content[:15]})>")  # first 15 chars

    def serialize(self) -> Dict[str, str]:
        return {"id": self.id,
                "author_id": self.author_id,
                "content": self.content,
                "created_at": self.created_at}
