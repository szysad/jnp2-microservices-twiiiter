from fastapi import FastAPI
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import session


app = FastAPI()
SessionLocal: session


def init_db() -> session:
    '''
        inits db connection, creates tables and returns session,
        NOTE: to talk to db, create instance of returned session
    '''

    # connecto to databae
    from . import config
    conn_url = (
        f"postgres://{config.DB_USER}:{config.DB_PASS}"
        f"@{config.DB_HOST}/{config.DB_DATABASE}")

    engine = create_engine(conn_url, echo=config.DEBUG)
    sess = sessionmaker(bind=engine)

    # create models
    from .models import Base
    Base.metadata.create_all(bind=engine)

    return sess


@app.on_event("startup")
def init_app() -> None:
    ''' inits db, and starts routing '''

    global SessionLocal
    SessionLocal = init_db()

    # start routing
    from .routes import router
    app.include_router(router)
