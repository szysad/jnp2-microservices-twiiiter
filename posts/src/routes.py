from .app import app
from .models import Post
from .schemas import PostIn
from .utils import session_scope, extract_id, InvalidTokenError
from fastapi import APIRouter, Response, status, Query, Header
from typing import List, Optional, Callable, Dict
from fastapi.responses import JSONResponse

router = APIRouter()


@router.post("/posts")
def add_post(post_data: PostIn, WWW_Authenticate: str = Header(...)):
    poster_id: int
    try:
        poster_id = extract_id(WWW_Authenticate)
    except InvalidTokenError as e:
        print(e)
        Response.status_code = status.HTTP_403_FORBIDDEN
        return {"error": "invalid token"}

    new_post = Post(**post_data.dict(), author_id=poster_id)
    with session_scope() as sess:
        sess.add(new_post)
        sess.flush()
        sess.refresh(new_post)
        return new_post.serialize()  # this has to be in sess_scope


@router.get("/posts")
def get_posts(id: Optional[List[int]] = Query(None),
              author_id: Optional[List[int]] = Query(None)):
    if id is None and author_id is None:
        Response.status_code = status.HTTP_400_BAD_REQUEST
        err = {"error":
               ("either query params 'id' or",
                "'author_id' must be specified")}
        return JSONResponse(err, status_code=400)

    if id is not None and author_id is not None:
        Response.status_code = status.HTTP_400_BAD_REQUEST
        err = {"error":
               ("both query params 'id' and 'author_id'",
                "must not be specified at the same time")}
        return JSONResponse(err, 400)

    with session_scope() as sess:
        posts: List[Post]
        if id is not None:
            posts = sess.query(Post).filter(Post.id.in_(id)).all()
        else:
            posts = sess.query(Post).filter(
                Post.author_id.in_(author_id)).all()

        return [post.serialize() for post in posts]
