# Posts service
 - enables adding posts
 - enables querying posts
***
# Endpoints
Note: In all POST requests `Content-Type` must be set to `application/json`
### POST /posts
Adds post with given `content` and post ownership is established on `WWW-Authenticate` header content.
Request data must be json with field `content` of type `string` whitch length must be greater then 0 and smaller then 280.
Additionally request must containt Header `WWW-Authenticate` with token received from service `users`

Example request:
```sh
$ CURL -X POST -H "Content-Type: application/json" -H "WWW-Authenticate: users.received.token" -d '{"content": "example post content"}'
```
Example response:
```json
{
    "id": 27,
    "author_id": 1,
    "content": "example post content",
    "created_at": "2020-08-05T18:35:40.325992"
}
```

### GET /posts
Not documented
