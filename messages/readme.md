# Messages service
 - enables adding messages
 - enables querying all messages to users
***
# Endpoints
Note: In all POST requests `Content-Type` must be set to `application/json`
### POST /messages
Adds message with given `content` and message author is established on `WWW-Authenticate` header content.
Request data must be json with field `content` of type `string` which length must be greater then 0 and smaller then 280.
Additionally request must contain Header `WWW-Authenticate` with token received from service `users` and receiver id.

Example request:
```sh
$ CURL -X POST -H "Content-Type: application/json" -H "WWW-Authenticate: users.received.token" -d '{"content": "example message content", "receiver_id": "1"}'
```

### GET /message
Returns list of messages sent to specified user. Each message is described:
```json
{
    "id": 27,
    "sender_id": 1,
    "receiver_id": 2,
    "content": "example post content",
    "created_at": "2020-08-05T18:35:40.325992"
}
```