from pydantic import BaseModel, validator
from .models import CONTENT_MAX_LEN


class MessageIn(BaseModel):
    content: str
    receiver_id: int

    @validator("content")
    def cant_be_empty_nor_too_long(cls, v):
        if len(v) == 0:
            raise ValueError("cant be empty")
        if len(v) > CONTENT_MAX_LEN:
            err_msg = f"cant be longer then {CONTENT_MAX_LEN} chars"
            raise ValueError(err_msg)
        return v