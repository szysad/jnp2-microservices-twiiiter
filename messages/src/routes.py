from .app import app
from .models import Message
from .schemas import MessageIn
from .utils import session_scope, extract_id, InvalidTokenError
from fastapi import APIRouter, Response, status, Query, Header
from typing import List, Optional, Callable, Dict
from fastapi.responses import JSONResponse

router = APIRouter()


@router.post("/messages")
def add_message(message_data: MessageIn, WWW_Authenticate: str = Header(...)):
    sender_id: int
    try:
        sender_id = extract_id(WWW_Authenticate)
    except InvalidTokenError as e:
        print(e)
        Response.status_code = status.HTTP_403_FORBIDDEN
        return {"error": "invalid token"}

    if sender_id == message_data.receiver_id:
        return {"error": "user can not send message to himself"}

    new_message = Message(**message_data.dict(), sender_id=sender_id)
    with session_scope() as sess:
        sess.add(new_message)
        sess.flush()
        sess.refresh(new_message)
        return new_message.serialize()


@router.get("/messages")
def get_messages(id: Optional[List[int]] = Query(None),
                 receiver_id: Optional[List[int]] = Query(None),
                 WWW_Authenticate: str = Header(...)):
    r_id: int
    if id is not None and receiver_id is not None:
        Response.status_code = status.HTTP_400_BAD_REQUEST
        err = {"error": "both query params 'id' or 'receiver_id' must not be specified at the same time"}
        return JSONResponse(err, 400)

    #if id is None and receiver_id is None:
    #    Response.status_code = status.HTTP_400_BAD_REQUEST
    #    err = {"error": "either query params 'id' or 'receiver_id' must be specified"}
    #    return JSONResponse(err, status_code=400)

    if id is None:
        try:
            r_id = extract_id(WWW_Authenticate)
        except InvalidTokenError as e:
            print(e)
            Response.status_code = status.HTTP_403_FORBIDDEN
            return {"error": "invalid token"}
        if receiver_id is None:
            receiver_id = [r_id]
        else:
            if r_id not in receiver_id:
                receiver_id.append(r_id)

    with session_scope() as sess:
        messages: List[Message]
        if id is not None:
            messages = sess.query(Message).filter(Message.id.in_(id)).all()
        else:
            messages = sess.query(Message).filter(Message.receiver_id.in_(receiver_id)).all()

        return [message.serialize() for message in messages]
