from contextlib import contextmanager
from .app import SessionLocal
from sqlalchemy.orm.session import Session
from .config import TOKEN_SECRET
import jwt
from jwt.exceptions import InvalidTokenError


@contextmanager
def session_scope() -> Session:
    '''provides transaction scope of session'''
    session = SessionLocal()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


def extract_id(token: str) -> int:
    ''' returns sub from token or raises Exception '''
    encoded = jwt.decode(token, TOKEN_SECRET, algorithm=["HS256"])
    sub: int
    if 'sub' not in encoded:
        raise InvalidTokenError("token doesnt have 'sub' key")
    try:
        sub = int(encoded['sub'])
    except Exception:
        raise InvalidTokenError("token sub value is not integer")
    return sub