from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa
from datetime import datetime
from typing import Dict

CONTENT_MAX_LEN = 280
Base = declarative_base()


class Message(Base):
    __tablename__ = 'messages'

    id = sa.Column(sa.INTEGER, primary_key=True)
    sender_id = sa.Column(sa.INTEGER, nullable=False)
    receiver_id = sa.Column(sa.INTEGER, nullable=False)
    content = sa.Column(sa.String(CONTENT_MAX_LEN), nullable=False)
    created_at = sa.Column(sa.DateTime, nullable=False)

    def __init__(self, 
                 sender_id: int,
                 receiver_id: int,
                 content: str):
        self.sender_id = sender_id
        self.receiver_id = receiver_id
        self.content = content
        self.created_at = datetime.now()

    def __repr__(self):
        return (f"Message<(id={self.id}, sender_id={self.sender_id}, receiver_id={self.receiver_id},",
                f"created_at={self.created_at},",
                f"content={self.content[:15]})>")

    def serialize(self) -> Dict[str, str]:
        return {"id": self.id,
                "sender_id": self.sender_id,
                "receiver_id": self.receiver_id,
                "content": self.content,
                "created_at": self.created_at}
