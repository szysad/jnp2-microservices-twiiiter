import os


TOKEN_SECRET = os.getenv("TOKEN_SECRET")
DEBUG = "DEBUG" in os.environ

# db config
DB_HOST = os.getenv("DB_HOST")  # for example users:2115
DB_PASS = os.getenv("DB_PASS")
DB_USER = os.getenv("DB_USER")
DB_DATABASE = os.getenv("DB_DATABASE")


def env_check():
    mandatory = [(DB_HOST, "DB_HOST"), (DB_PASS, "DB_PASS"),
                 (DB_USER, "DB_USER"), (DB_DATABASE, "DB_DATABASE"),
                 (TOKEN_SECRET, "TOKEN_SECRET")]
    for v in mandatory:
        if v[0] is None:
            raise Exception(f"mandatory variable {v[1]} not declared")


env_check()
